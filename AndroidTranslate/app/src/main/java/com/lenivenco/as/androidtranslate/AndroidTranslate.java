package com.lenivenco.as.androidtranslate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class AndroidTranslate extends AppCompatActivity {
    EditText MyInputText;
    Button MyTranslateButton;
    TextView MyOutputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_translate);

            MyInputText = (EditText)findViewById(R.id.InputText);
            MyTranslateButton = (Button)findViewById(R.id.TranslateButton);
            MyOutputText = (TextView)findViewById(R.id.OutputText);

            MyTranslateButton.setOnClickListener(MyTranslateButtonOnClickListener);
        }

        private Button.OnClickListener MyTranslateButtonOnClickListener
                = new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String InputString;
                String OutputString = null;
                InputString = MyInputText.getText().toString();

                try {
                    Translate.setHttpReferrer("http://android-er.blogspot.com/");
                    OutputString = Translate.execute(InputString,
                            Language.ENGLISH, Language.FRENCH);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    OutputString = "Error";
                }

                MyOutputText.setText(OutputString);

            }

        };

    }
