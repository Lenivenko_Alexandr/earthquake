package com.lenivenco.as.earthquake;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;

import java.net.URL;
import java.util.List;

public class EarthquakeLoader extends AsyncTaskLoader<List<EarthquakeInfo>> {
    private String mUrl;

    public EarthquakeLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<EarthquakeInfo> loadInBackground() {

        if (mUrl == null) {
            return null;
        }

        // Perform the network request, parse the response, and extract a list of earthquakes.
        List<EarthquakeInfo> earthquakes = QueryUtils.extractEarthquakes(mUrl);
        return earthquakes;
    }
}



