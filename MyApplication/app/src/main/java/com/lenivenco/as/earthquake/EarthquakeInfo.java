package com.lenivenco.as.earthquake;


import android.net.Uri;

import java.net.URL;

public class EarthquakeInfo {
    private Uri url = null;
    private double mPower;
    private String mCityName;
    private Long mDate;


    public EarthquakeInfo() {
        this(0,null,0);
    }

    public EarthquakeInfo(double mPower, String mCityName, long mDate) {
        this.mPower = mPower;
        this.mCityName = mCityName;
        this.mDate = mDate;
    }

    public double getPower() {
        return mPower;
    }

    public void setPower(double mPower) {
        this.mPower = mPower;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String mCityName) {
        this.mCityName = mCityName;
    }

    public long getTimeInMilliseconds() {
        return mDate;
    }

    public void setDate(long mDate) {
        this.mDate = mDate;
    }

    public Uri getUri() {
        return url;
    }

    public void setUrl(String url) {
        this.url = Uri.parse(url);
    }
}
